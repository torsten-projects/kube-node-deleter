package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/akamensky/argparse"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func getClient() *kubernetes.Clientset {
	config, err := rest.InClusterConfig()
	if err != nil {
		log.Print("Unable to read in-cluster kubeconfig, trying ~/.kube/config")
		config, err = clientcmd.BuildConfigFromFlags("", fmt.Sprintf("%v/.kube/config", homedir.HomeDir()))
		check(err)
	}
	clientset, err := kubernetes.NewForConfig(config)
	check(err)
	return clientset
}

func deleteOldNotReadyNodes(client *kubernetes.Clientset) {
	log.Printf("Checking NotReady nodes")
	nodes, err := client.CoreV1().Nodes().List(context.TODO(), metav1.ListOptions{})
	check(err)

	// Iterate through the nodes and delete any that are in the "NotReady" state
	// and have been in that state for at least 5 minutes
	for _, node := range nodes.Items {
		for _, condition := range node.Status.Conditions {
			if condition.Type == "Ready" && (condition.Status == "False" || condition.Status == "Unknown") {
				if time.Since(condition.LastTransitionTime.Time) >= 5*time.Minute {
					log.Printf("Deleting node %s\n", node.Name)
					err := client.CoreV1().Nodes().Delete(context.TODO(), node.Name, metav1.DeleteOptions{})
					check(err)
				} else {
					log.Printf("Node transition too fresh: %v", node.Name)
				}
			}
		}
	}
}

func checkSetLabel(nodeName string, labelMap map[string]string, key string, value string) bool {
	curValue, ok := labelMap[key]
	if ok && curValue == value {
		return false
	} else {
		log.Printf("Adding label %v=%v to node %v", key, value, nodeName)
		labelMap[key] = value
		return true
	}
}

func relabelMasterNodes(client *kubernetes.Clientset, masterNodes *[]string) {
	log.Printf("Checking node labels")
	for _, nodeName := range *masterNodes {
		node, err := client.CoreV1().Nodes().Get(context.TODO(), nodeName, metav1.GetOptions{})

		modified := false

		check(err)
		if node.Labels == nil {
			modified = true
			node.Labels = make(map[string]string)
		}

		if checkSetLabel(nodeName, node.Labels, "node-role.kubernetes.io/control-plane", "control-plane") {
			modified = true
		}
		if checkSetLabel(nodeName, node.Labels, "node-role.kubernetes.io/master", "master") {
			modified = true
		}

		if modified {
			log.Printf("Node %v's labels modified, updating", nodeName)
			_, err = client.CoreV1().Nodes().Update(context.TODO(), node, metav1.UpdateOptions{})
			check(err)
		}

	}
}

func main() {
	parser := argparse.NewParser("deleter", "Deletes any stuck node in NotReady state for longer than 5 minutes")
	masterNodes := parser.StringList("m", "master", nil)
	err := parser.Parse(os.Args)
	check(err)

	// Load in-cluster configuration
	client := getClient()

	for _, masterNode := range *masterNodes {
		log.Printf("Master node: %v", masterNode)
	}

	for {
		deleteOldNotReadyNodes(client)
		relabelMasterNodes(client, masterNodes)
		time.Sleep(30 * time.Second)
	}

}
